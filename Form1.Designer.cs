﻿namespace Tenkaichi3CoreCharEdittor
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.loadCharacterButton = new System.Windows.Forms.Button();
            this.characterFile_TextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.kiChargeComboBox = new System.Windows.Forms.ComboBox();
            this.kiChargeVoiceComboBox = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.contactFallSoundComboBox = new System.Windows.Forms.ComboBox();
            this.auraColorComboBox = new System.Windows.Forms.ComboBox();
            this.baselineKiComboBox = new System.Windows.Forms.ComboBox();
            this.kiChargeSpeedComboBox = new System.Windows.Forms.ComboBox();
            this.kiChargeSpeedUnderWaterComboBox = new System.Windows.Forms.ComboBox();
            this.baselineKiRechargeComboBox = new System.Windows.Forms.ComboBox();
            this.guardBreakKiRecoverComboBox = new System.Windows.Forms.ComboBox();
            this.blastBallRegenComboBox = new System.Windows.Forms.ComboBox();
            this.maxPowerModeLengthComboBox = new System.Windows.Forms.ComboBox();
            this.firstFinisherComboBox = new System.Windows.Forms.ComboBox();
            this.secondFinisherComboBox = new System.Windows.Forms.ComboBox();
            this.thirdFinishercomboBox = new System.Windows.Forms.ComboBox();
            this.fourthFinishercomboBox = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.finisher = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.finisher1stLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // loadCharacterButton
            // 
            this.loadCharacterButton.Location = new System.Drawing.Point(16, 57);
            this.loadCharacterButton.Name = "loadCharacterButton";
            this.loadCharacterButton.Size = new System.Drawing.Size(140, 23);
            this.loadCharacterButton.TabIndex = 0;
            this.loadCharacterButton.Text = "Load Character File";
            this.loadCharacterButton.UseVisualStyleBackColor = true;
            // 
            // characterFile_TextBox
            // 
            this.characterFile_TextBox.Location = new System.Drawing.Point(16, 17);
            this.characterFile_TextBox.Name = "characterFile_TextBox";
            this.characterFile_TextBox.Size = new System.Drawing.Size(140, 20);
            this.characterFile_TextBox.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(34, 110);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Ki Charge Traits";
            // 
            // kiChargeComboBox
            // 
            this.kiChargeComboBox.FormattingEnabled = true;
            this.kiChargeComboBox.Location = new System.Drawing.Point(15, 126);
            this.kiChargeComboBox.Name = "kiChargeComboBox";
            this.kiChargeComboBox.Size = new System.Drawing.Size(121, 21);
            this.kiChargeComboBox.TabIndex = 3;
            // 
            // kiChargeVoiceComboBox
            // 
            this.kiChargeVoiceComboBox.FormattingEnabled = true;
            this.kiChargeVoiceComboBox.Location = new System.Drawing.Point(188, 125);
            this.kiChargeVoiceComboBox.Name = "kiChargeVoiceComboBox";
            this.kiChargeVoiceComboBox.Size = new System.Drawing.Size(121, 21);
            this.kiChargeVoiceComboBox.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(209, 109);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(83, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Ki Charge Voice";
            // 
            // contactFallSoundComboBox
            // 
            this.contactFallSoundComboBox.FormattingEnabled = true;
            this.contactFallSoundComboBox.Location = new System.Drawing.Point(373, 125);
            this.contactFallSoundComboBox.Name = "contactFallSoundComboBox";
            this.contactFallSoundComboBox.Size = new System.Drawing.Size(121, 21);
            this.contactFallSoundComboBox.TabIndex = 6;
            // 
            // auraColorComboBox
            // 
            this.auraColorComboBox.FormattingEnabled = true;
            this.auraColorComboBox.Location = new System.Drawing.Point(15, 199);
            this.auraColorComboBox.Name = "auraColorComboBox";
            this.auraColorComboBox.Size = new System.Drawing.Size(121, 21);
            this.auraColorComboBox.TabIndex = 7;
            // 
            // baselineKiComboBox
            // 
            this.baselineKiComboBox.FormattingEnabled = true;
            this.baselineKiComboBox.Location = new System.Drawing.Point(563, 126);
            this.baselineKiComboBox.Name = "baselineKiComboBox";
            this.baselineKiComboBox.Size = new System.Drawing.Size(121, 21);
            this.baselineKiComboBox.TabIndex = 8;
            // 
            // kiChargeSpeedComboBox
            // 
            this.kiChargeSpeedComboBox.FormattingEnabled = true;
            this.kiChargeSpeedComboBox.Location = new System.Drawing.Point(188, 199);
            this.kiChargeSpeedComboBox.Name = "kiChargeSpeedComboBox";
            this.kiChargeSpeedComboBox.Size = new System.Drawing.Size(121, 21);
            this.kiChargeSpeedComboBox.TabIndex = 9;
            // 
            // kiChargeSpeedUnderWaterComboBox
            // 
            this.kiChargeSpeedUnderWaterComboBox.FormattingEnabled = true;
            this.kiChargeSpeedUnderWaterComboBox.Location = new System.Drawing.Point(373, 199);
            this.kiChargeSpeedUnderWaterComboBox.Name = "kiChargeSpeedUnderWaterComboBox";
            this.kiChargeSpeedUnderWaterComboBox.Size = new System.Drawing.Size(121, 21);
            this.kiChargeSpeedUnderWaterComboBox.TabIndex = 10;
            // 
            // baselineKiRechargeComboBox
            // 
            this.baselineKiRechargeComboBox.FormattingEnabled = true;
            this.baselineKiRechargeComboBox.Location = new System.Drawing.Point(563, 199);
            this.baselineKiRechargeComboBox.Name = "baselineKiRechargeComboBox";
            this.baselineKiRechargeComboBox.Size = new System.Drawing.Size(121, 21);
            this.baselineKiRechargeComboBox.TabIndex = 11;
            // 
            // guardBreakKiRecoverComboBox
            // 
            this.guardBreakKiRecoverComboBox.FormattingEnabled = true;
            this.guardBreakKiRecoverComboBox.Location = new System.Drawing.Point(15, 278);
            this.guardBreakKiRecoverComboBox.Name = "guardBreakKiRecoverComboBox";
            this.guardBreakKiRecoverComboBox.Size = new System.Drawing.Size(121, 21);
            this.guardBreakKiRecoverComboBox.TabIndex = 12;
            // 
            // blastBallRegenComboBox
            // 
            this.blastBallRegenComboBox.FormattingEnabled = true;
            this.blastBallRegenComboBox.Location = new System.Drawing.Point(188, 277);
            this.blastBallRegenComboBox.Name = "blastBallRegenComboBox";
            this.blastBallRegenComboBox.Size = new System.Drawing.Size(121, 21);
            this.blastBallRegenComboBox.TabIndex = 13;
            // 
            // maxPowerModeLengthComboBox
            // 
            this.maxPowerModeLengthComboBox.FormattingEnabled = true;
            this.maxPowerModeLengthComboBox.Location = new System.Drawing.Point(373, 277);
            this.maxPowerModeLengthComboBox.Name = "maxPowerModeLengthComboBox";
            this.maxPowerModeLengthComboBox.Size = new System.Drawing.Size(121, 21);
            this.maxPowerModeLengthComboBox.TabIndex = 14;
            // 
            // firstFinisherComboBox
            // 
            this.firstFinisherComboBox.FormattingEnabled = true;
            this.firstFinisherComboBox.Location = new System.Drawing.Point(563, 277);
            this.firstFinisherComboBox.Name = "firstFinisherComboBox";
            this.firstFinisherComboBox.Size = new System.Drawing.Size(121, 21);
            this.firstFinisherComboBox.TabIndex = 15;
            // 
            // secondFinisherComboBox
            // 
            this.secondFinisherComboBox.FormattingEnabled = true;
            this.secondFinisherComboBox.Location = new System.Drawing.Point(15, 351);
            this.secondFinisherComboBox.Name = "secondFinisherComboBox";
            this.secondFinisherComboBox.Size = new System.Drawing.Size(121, 21);
            this.secondFinisherComboBox.TabIndex = 16;
            // 
            // thirdFinishercomboBox
            // 
            this.thirdFinishercomboBox.FormattingEnabled = true;
            this.thirdFinishercomboBox.Location = new System.Drawing.Point(188, 351);
            this.thirdFinishercomboBox.Name = "thirdFinishercomboBox";
            this.thirdFinishercomboBox.Size = new System.Drawing.Size(121, 21);
            this.thirdFinishercomboBox.TabIndex = 17;
            // 
            // fourthFinishercomboBox
            // 
            this.fourthFinishercomboBox.FormattingEnabled = true;
            this.fourthFinishercomboBox.Location = new System.Drawing.Point(373, 350);
            this.fourthFinishercomboBox.Name = "fourthFinishercomboBox";
            this.fourthFinishercomboBox.Size = new System.Drawing.Size(121, 21);
            this.fourthFinishercomboBox.TabIndex = 18;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(370, 109);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(111, 13);
            this.label3.TabIndex = 20;
            this.label3.Text = "Falling/Contact sound";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(590, 109);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 13);
            this.label4.TabIndex = 21;
            this.label4.Text = "Baseline Ki";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(37, 180);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 13);
            this.label5.TabIndex = 22;
            this.label5.Text = "Aura Color";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(205, 180);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(87, 13);
            this.label6.TabIndex = 23;
            this.label6.Text = "Ki Charge Speed";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(360, 180);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(145, 13);
            this.label7.TabIndex = 24;
            this.label7.Text = "Ki Charge Speed Underwater";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(560, 180);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(135, 13);
            this.label8.TabIndex = 25;
            this.label8.Text = "Baseline Ki Recharge Rate";
            // 
            // finisher
            // 
            this.finisher.AutoSize = true;
            this.finisher.Location = new System.Drawing.Point(37, 332);
            this.finisher.Name = "finisher";
            this.finisher.Size = new System.Drawing.Size(64, 13);
            this.finisher.TabIndex = 26;
            this.finisher.Text = "2nd Finisher";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(212, 331);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(61, 13);
            this.label10.TabIndex = 27;
            this.label10.Text = "3rd Finisher";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(404, 332);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(61, 13);
            this.label11.TabIndex = 28;
            this.label11.Text = "4th Finisher";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(2, 259);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(154, 13);
            this.label12.TabIndex = 29;
            this.label12.Text = "Ki Recovery Rate Guard Break";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(195, 259);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(111, 13);
            this.label13.TabIndex = 30;
            this.label13.Text = "Blast Ball Regen Rate";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(385, 259);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(96, 13);
            this.label14.TabIndex = 31;
            this.label14.Text = "Max Power Length";
            // 
            // finisher1stLabel
            // 
            this.finisher1stLabel.AutoSize = true;
            this.finisher1stLabel.Location = new System.Drawing.Point(590, 259);
            this.finisher1stLabel.Name = "finisher1stLabel";
            this.finisher1stLabel.Size = new System.Drawing.Size(60, 13);
            this.finisher1stLabel.TabIndex = 32;
            this.finisher1stLabel.Text = "1st Finisher";
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(796, 454);
            this.Controls.Add(this.finisher1stLabel);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.finisher);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.fourthFinishercomboBox);
            this.Controls.Add(this.thirdFinishercomboBox);
            this.Controls.Add(this.secondFinisherComboBox);
            this.Controls.Add(this.firstFinisherComboBox);
            this.Controls.Add(this.maxPowerModeLengthComboBox);
            this.Controls.Add(this.blastBallRegenComboBox);
            this.Controls.Add(this.guardBreakKiRecoverComboBox);
            this.Controls.Add(this.baselineKiRechargeComboBox);
            this.Controls.Add(this.kiChargeSpeedUnderWaterComboBox);
            this.Controls.Add(this.kiChargeSpeedComboBox);
            this.Controls.Add(this.baselineKiComboBox);
            this.Controls.Add(this.auraColorComboBox);
            this.Controls.Add(this.contactFallSoundComboBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.kiChargeVoiceComboBox);
            this.Controls.Add(this.kiChargeComboBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.characterFile_TextBox);
            this.Controls.Add(this.loadCharacterButton);
            this.Name = "MainWindow";
            this.Text = "Tenkaichi 3 Core Character Editor";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button loadCharacterButton;
        private System.Windows.Forms.TextBox characterFile_TextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox kiChargeComboBox;
        private System.Windows.Forms.ComboBox kiChargeVoiceComboBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox contactFallSoundComboBox;
        private System.Windows.Forms.ComboBox auraColorComboBox;
        private System.Windows.Forms.ComboBox baselineKiComboBox;
        private System.Windows.Forms.ComboBox kiChargeSpeedComboBox;
        private System.Windows.Forms.ComboBox kiChargeSpeedUnderWaterComboBox;
        private System.Windows.Forms.ComboBox baselineKiRechargeComboBox;
        private System.Windows.Forms.ComboBox guardBreakKiRecoverComboBox;
        private System.Windows.Forms.ComboBox blastBallRegenComboBox;
        private System.Windows.Forms.ComboBox maxPowerModeLengthComboBox;
        private System.Windows.Forms.ComboBox firstFinisherComboBox;
        private System.Windows.Forms.ComboBox secondFinisherComboBox;
        private System.Windows.Forms.ComboBox thirdFinishercomboBox;
        private System.Windows.Forms.ComboBox fourthFinishercomboBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label finisher;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label finisher1stLabel;
    }
}

